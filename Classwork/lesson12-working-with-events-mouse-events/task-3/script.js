/**
 * При наведенні на блок 1 робити
 * блок 2 зеленим кольором
 * А при наведенні на блок 2 робити
 * блок 1 червоним кольором
 *
 */
const blockEl1 = document.querySelector('#block-1');
const blockEl2 = document.querySelector('#block-2');

blockEl1.addEventListener('mouseenter', function(event){
    blockEl2.style.backgroundColor = 'green'
})
blockEl1.addEventListener('mouseleave', function(event){
    blockEl2.style.backgroundColor = 'white'
})
blockEl2.addEventListener('mouseenter', function(event){
    blockEl1.style.backgroundColor = 'red'
})
blockEl2.addEventListener('mouseleave', function(event){
    blockEl1.style.backgroundColor = 'white'
})