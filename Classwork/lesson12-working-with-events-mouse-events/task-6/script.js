/**
 * Початкове значення кнопки повинно дорівнювати 0
 * При натисканні на кнопку збільшувати це значення на 1
 *
 */
const btn = document.querySelector('.counter');
let n = 1
btn.innerText = 0;


btn.addEventListener('click', function(event){
    btn.innerText = n;
    if (n<=0){
    n = n+1;
    }
})
