/**
 * Початкове значення лічильника 0
 * При натисканні на + збільшувати лічильник на 1
 * При натисканні на - зменшувати лічильник на 1
 *
 * ADVANCED: не давати можливості задавати лічильник менше 0
 *
 */
const counter = document.querySelector('#counter');
const plus = document.querySelector('#increment-btn');
const minus = document.querySelector ('#decrement-btn');

let num =0

plus.addEventListener('click', function(event){
    counter.innerText = `Counter: ${num}`
    num = num+1;
    
})
minus.addEventListener('click', function(event){
   
   if (num>0){
    num = num-1;
   }
   counter.innerText = `Counter: ${num}`
})
