/**
 * Показувати параграф тільки при наведенні на
 * заголовок
 *
 */
const heading = document.querySelector('h1');
const paragraph = document.querySelector('p');
paragraph.style.display = 'none';
heading.addEventListener('mouseenter', function(event){
    paragraph.style.display = 'block';
})
heading.addEventListener('mouseleave', function(event){
    paragraph.style.display = 'none';
})