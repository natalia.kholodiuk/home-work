/**
 * При натисканні на кнопку Add додавати
 * квадрат в блок squares-container
 * Для квадрату стилі вже є наявні
 *
 * ADVANCED: при додаванні задавати колір квадрату випадковим чином
 *
 */

const buttonEl = document.querySelector('#add-btn');
const divEl = document.querySelector('.squares-container');

buttonEl.addEventListener('click', function(event){
    const block = document.createElement('div');
    block.classList.add('square');
    block.style = "background-color: " + 
    '#' + (Math.random().toString(16) + '000000').substring(2,8).toUpperCase()
  ;
    divEl.append(block)
})