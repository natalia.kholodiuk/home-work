/**
 * Зробити список покупок з можливістю
 * додавати нові товари
 *
 * В поле вводу ми можемо ввести назву нового елементу
 * для списку покупок
 * При натисканні на кнопку Add введене значення
 * потрібно додавати в кінець списку покупок, а
 * поле вводу очищувати
 *
 * ADVANCED: Додати можливість видаляти елементи зі списку покупок
 * (додати Х в кінець кожного елементу, при кліку на який відбувається
 * видалення) та відмічати як уже куплені (елемент перекреслюється при
 * кліку на нього)
 * Для реалізації цього потрібно розібратися з Event delegation
 * https://javascript.info/event-delegation
 */

 const inputEl = document.querySelector('#new-good-input');
 const btnEl = document.querySelector('#add-btn')
 const shopList = document.querySelector('.shopping-list')
 
 btnEl.addEventListener('click',function(element){
     let value = inputEl.value;
     let shopEl = document.createElement('li');
        shopEl.innerText = value;
        shopList.append(shopEl)
        inputEl.value = ""
    let target = event.target('li');
    if (target){}
     
 })