/**
 * Додати на сторінку поле вводу (input) та кнопку Show
 * При натисканні на кнопку Validate відображати
 * VALID зеленим кольром, якщо значення проходить валідацію
 * INVALID червоним кольором, якщо значення не проходить валідацію
 *
 * Правила валідації значення:
 * - значення не пусте
 *
 * ADVANCED
 * Правила валідації значення:
 * - повинно містити щонайменше 5 символів
 * - не повинно містити пробілів
 * - повинно починатися з літери (потрібно використати регулярні вирази)
 *
 */
const text = document.querySelector ('#input');
const btn = document.querySelector ('#validate-btn');
const checkValid = document.createElement('p')
btn.after(checkValid)

btn.addEventListener('click', function(event){
    let value = text.value;
console.log(value);
  if (value&&value.length>=5&&value.trim!==''){
       checkValid.innerText = 'valid';
        checkValid.style.color = 'green'}
  else {checkValid.innerText = 'invalid';
       checkValid.style.color = 'red'}
})