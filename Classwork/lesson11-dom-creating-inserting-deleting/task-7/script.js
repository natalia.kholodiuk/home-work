/**
 * Написати функцію createChessBoard, якій можна передавати розмір дошки,
 * а в результаті на сторінці утворюється шахматна дошка розміром 8х8
 */
// let body = document.body;
// let flag = true;
// function createChessBoard(size) {
//     let board = document.createElement('table');
//     body.prepend(board);
//     board.style.width = `${size}px`;
//     board.style.height = `${size}px`;
//     board.style.padding = '0';
//     board.style.border = '1px solid black';    
//     for (let j=0; j<8; j++)
//     {   let tr = document.createElement('tr');
//         tr.style.width = `${size}px`;
//         tr.style.height = `${size/8-2}px`;
//         board.prepend(tr);
//         for (let i=0; i<8; i++)
//             {let td = document.createElement('td');
//             td.style.width = `${size/8-2}px`;
//             td.style.height = tr.style.height;
//             td.style.padding = '0';
//             td.style.border = '1px solid black';
//             tr.prepend(td);    
//             if (i==0){flag=!flag}
//             if (flag){td.style.backgroundColor = 'black'}
//             else {td.style.backgroundColor = 'white'}
//             flag=!flag;
//             }
//     };       
// }
// createChessBoard(500);

let body = document.body;
function createChessBoard(size) {
    let board = document.createElement('table');
    body.prepend(board);
    board.style.width = `${size}px`;
    board.style.height = `${size}px`;
    board.style.padding = '0';
    board.style.border = '1px solid black';    
    for (let j=0; j<8; j++)
    {   let tr = document.createElement('tr');
        tr.style.width = `${size}px`;
        tr.style.height = `${size/8-2}px`;
        board.prepend(tr);
        for (let i=0; i<8; i++)
            {let td = document.createElement('td');
            td.style.width = `${size/8-2}px`;
            td.style.height = tr.style.height;
            td.style.padding = '0';
            td.style.border = '1px solid black';
            tr.prepend(td);    
            if (i%2===0&&j%2===0||i%2!==0&&j%2!==0){td.style.backgroundColor = 'black'};
            //if (i%2!==0&&j%2!==0){td.style.backgroundColor = 'black'}
            }
    };       
}
createChessBoard(500);