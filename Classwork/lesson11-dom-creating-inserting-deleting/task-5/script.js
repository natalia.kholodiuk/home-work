/**
 * 
 * Відобразити заданий список автомобілів у вигляді "НАЗВА МОДЕЛЬ",
 * електрокари виділити зеленим кольором, а інші авто - сірим, наприклад
 * Tesla S повинна бути зеленим кольором у списку
 * 
 */

const cars = [
    {
        name: 'Tesla',
        model: 'S',
        isElectric: true,
    },
    {
        name: 'Nissan',
        model: 'Leaf',
        isElectric: true,
    },
    {
        name: 'BMW',
        model: 'X3',
        isElectric: false,
    },
    {
        name: 'BMW',
        model: 'i3',
        isElectric: true,
    },
    {
        name: 'Toyota',
        model: 'Camry',
        isElectric: false,
    }
]
let list = document.createElement('ul');
cars.map(item => {
    let car = document.createElement('li')
    car.innerText = item.name+' '+item.model
    list.append(car);
    if(item.isElectric){car.style.color = 'green'}});
let body = document.querySelector('body');
body.prepend(list)
