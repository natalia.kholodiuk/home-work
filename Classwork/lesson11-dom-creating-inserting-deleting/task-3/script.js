/**
 * До наявного списку покупок додати в кінець Fish та Chocolate
 */

let list = document.querySelector('ul');
let choco = document.createElement('li');
choco.innerText = 'Chocolate';
let fish = document.createElement('li');
fish.innerText = 'Fish';
list.append(fish);
list.append(choco);

/**
 * Після першого фрукту в списку покупоку додати
 * Apple з класом fruits
 */
let app = document.createElement('li');
app.innerText = 'Apple';
app.classList.add('fruits');
let fruit = document.querySelectorAll('.fruits');
fruit[0].after(app)

/**
 * Нижче списку покупок додати список магазинів з класом groceries
 */
let shops = document.createElement ('ul');
shops.classList.add('groceries')
list.after(shops)


/**
 * До списку магазинів додати Novus, Silpo, ATB, Trash, Lotok
 * Сільпо та Новус повинні бути виділені як преміум (додати клас premium-store)
 *
 * Оптимальніше буде використання методу insertAdjacentHTML
 * https://developer.mozilla.org/en-US/docs/Web/API/Element/insertAdjacentHTML
 */
shops.insertAdjacentHTML('afterbegin', '<li>Novus</li> <li>Silpo</li> <li>ATB</li><li>Trash</li><li>Lotok</li>' );
let shopElem = document.querySelectorAll('.groceries li');
shopElem[0].classList.add('premium-store');
shopElem[1].classList.add('premium-store');
console.log(shops);