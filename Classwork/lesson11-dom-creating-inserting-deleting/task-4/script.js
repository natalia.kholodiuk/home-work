/**
 * Відобразити список фруктів на сторінці
 */

const fruitsList = ['pear', 'cherry', 'avocado', 'apple', 'lemon', 'mango', 'peach', 'strawberry', 'coconut', 'papaya', 'grape', 'nectarine', 'grapefruit'];
let body = document.querySelector('body')
body.insertAdjacentHTML('afterbegin', `${fruitsList}`);
const list = document.createElement('ul');
body.prepend(list);
for (const item of fruitsList){
    let fruit = document.createElement('li');
    fruit.innerText = item;
    list.prepend(fruit)
}