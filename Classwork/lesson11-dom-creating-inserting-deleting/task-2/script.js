/**
 *
 * Дати користувачу можливість створювати квадрат на сторінці
 *
 * Користувач повинен мати змогу ввести розмір квадрату та колір
 * А в результаті на сторінці повинен відобразитися квадрат
 *
 * Наприклад, користувач спочатку вводить 50, потім red
 * і на сторінці з'являється червоний квадрат розміром 50 пкс
 *
 */
let length = +prompt('Enter the size');
let color = prompt ('Enter the color');
let body = document.querySelector('body');
let div = document.createElement ('div');
div.style.border = '2px solid black'
div.style.width = `${length}px`;
div.style.height = `${length}px`;
div.style.backgroundColor = color;
body.prepend(div);
console.log(div);
