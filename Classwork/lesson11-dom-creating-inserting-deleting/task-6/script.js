/**
 * Наявний список завдань відобразити на сторінці у вигляді 
 * трьох списків в залежності від пріорітетності
 * 
 * Елементи повинні бути наступними кольорами
 * високою пріорітетностю - червоним
 * середньою - оранжевим
 * низькою - блакитним
 * 
 */

const tasks = [
    {
        name: 'Create website',
        priority: 'high',
    },
    {
        name: 'Hire new manager',
        priority: 'low',
    },
    {
        name: 'Help co-worker',
        priority: 'low',
    },
    {
        name: 'Make a report',
        priority: 'medium',
    },
    {
        name: 'Provide payment',
        priority: 'high',
    },
    {
        name: 'Perform review',
        priority: 'low',
    },
    {
        name: 'Clean the workspace',
        priority: 'medium'
    }
]
let high = document.createElement('ul')
let medium = document.createElement('ul')
let low = document.createElement('ul')
tasks.map(item=>{
    let taskEl = document.createElement('li');
    taskEl.innerText = item.name
    if (item.priority === 'high')
    {high.append(taskEl); taskEl.style.color = 'red'};
    if (item.priority === 'medium')
    {medium.append(taskEl); taskEl.style.color = 'orange'}
    if (item.priority === 'low')
    {low.append(taskEl);taskEl.style.color = 'blue'}
   
})
let body = document.querySelector('body');
body.prepend(high, medium, low)