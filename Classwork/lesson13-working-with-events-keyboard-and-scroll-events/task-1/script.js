/**
 * В блок виводити назву натиснутої клавіші
 *
 * ADVANCED: виводити також комбінації Shift + key,
 * Ctrl + key, Alr + key
 * Наприклад при натисканні Shift + v у блоці
 * відобразити "Shift + v"
 *
 */
let pressedEl = document.querySelector('.pressed-info');
window.addEventListener('keypress',function(event){
    console.log(event);
    pressedEl.innerText = event.key
    if(event.ctrlKey){ 
        pressedEl.innerText = `Ctrl + ${event.key}`}
})