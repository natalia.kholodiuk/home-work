/**
 * За допомогою стрілок передвигати виділений
 * квадрат по клітинкам
 * При натисканні на пробіл випадковим чином переміщувати
 * активний квадрат
 *
 * ADVANCED:
 *
 * - направляти пакмена в сторону руху
 * Наприклад, якщо натиснули стрілку вверх, то смайл повинен бути
 * направлений вверх
 *
 * - дати користувачу можливість визначати розмір сітки
 * Наприклад, користувач вводить 10 і формується сітка 10х10,
 * випадково розміщується активний квадрат, а вся попередня
 * логіка щодо пересування зберігається
 * P.S. для цього потрібно очистити в html елемент board та генерувати
 * його повністю в js
 *
 */

