/**
 * Рахувати кількість натискань на пробіл, ентер,
 * шифт та альт клавіші
 * Відображати результат на сторінці
 *
 * ADVANCED: створити функцію, яка приймає тільки
 * назву клавіші, натискання якої потрібно рахувати,
 * а сам лічильник знаходиться в замиканні цієї функції
 * (https://learn.javascript.ru/closure)
 * id елемента, куди відображати результат має назву
 * "KEY-counter"
 *
 * Наприклад виклик функції
 * createCounter('Enter');
 * реалізовує логіку підрахунку натискання клавіші Enter
 * та відображає результат в enter-counter блок
 *
 */
let num = 0
let enterCounter = document.querySelector('#enter-counter')
window.addEventListener('keyup', function(event){
    console.log(event);
    num = num+1;
    if (event.code === 'Enter'){        
        enterCounter.innerText = num;
    }
})